# Overview
Singleton restricts a variable or object to one instance across the whole system.

# Advantages
- Allows a global setting or feature to be shared and managed in once place.

# Disadvantages
- It can introduce problems such as bottlenecks for multi-threaded applications.
- Singletons are not thread-safe without providing a your own locking mechanism.
- They have the same types of problems that you would get with global variables.
