#ifndef SYSTEMLOGGER_HPP
#define SYSTEMLOGGER_HPP

#include "Logger.hpp"

class SystemLogger
{
public:
    SystemLogger()
    {
    }

    static void Initialise(std::string const& filename)
    {
        GetLogger().Initialise(filename);
    }

    static std::string GetFilename()
    {
        return GetLogger().GetFilename();
    }

    static void Write(std::string const& message)
    {
        GetLogger().Write(message);
    }

private:
    static Logger& GetLogger()
    {
        static Logger logger;
        return logger;
    }
};

#endif // SYSTEMLOGGER_HPP
