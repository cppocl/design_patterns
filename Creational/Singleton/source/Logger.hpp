#ifndef LOGGER_HPP
#define LOGGER_HPP

#include <iostream>
#include <fstream>
#include <string>
#include <mutex>

class Logger
{
public:
    Logger()
    {
    }

    ~Logger()
    {
        Close();
    }

    void Initialise(std::string const& filename)
    {
        std::lock_guard<std::mutex> lock(m_lock);

        // Close the file when the filename has changed.
        bool close_file = !m_filename.empty() && m_filename != filename;
        if (close_file && m_file.is_open())
            m_file.close();

        m_filename = filename;
        m_file.open(filename);
    }

    std::string GetFilename() const
    {
        std::lock_guard<std::mutex> lock(m_lock);
        return m_filename;
    }

    void Close()
    {
        std::lock_guard<std::mutex> lock(m_lock);
        m_file.close();
    }

    void Write(std::string const& message)
    {
        std::lock_guard<std::mutex> lock(m_lock);

        if (!message.empty())
        {
            bool add_new_line = message[message.length() - 1] != '\n';

            std::cout << message;
            if (add_new_line)
                std::cout << std::endl;

            m_file.write(message.c_str(), message.length());
            if (add_new_line)
                m_file.write("\n", 1);
        }
    }

private:
    mutable std::mutex m_lock;

    std::ofstream m_file;
    std::string m_filename;

};

#endif // LOGGER_HPP
