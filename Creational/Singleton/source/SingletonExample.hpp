#ifndef SINGLETONEXAMPLE_HPP
#define SINGLETONEXAMPLE_HPP

#include "SingletonApplication.hpp"

class SingletonExample
{
public:
    static void Main(std::string const& filename)
    {
        SingletonApplication app1(filename);
        SingletonApplication app2;

        app1.Log("app1: A message\n");
        app2.Log("app2: Another message\n");
    }
};

#endif // SINGLETONEXAMPLE_HPP
