#ifndef APPLICATION_HPP
#define APPLICATION_HPP

#include "SystemLogger.hpp"
#include <cassert>

class SingletonApplication
{
public:
    SingletonApplication()
    {
        // Should never be the first overloaded constructor to be called.
        // Filename for logger must be set before sharing the logger.
        assert(!m_logger.GetFilename().empty());
    }

    // First time the application is initialised, set the log filename.
    SingletonApplication(std::string const& filename)
    {
        m_logger.Initialise(filename);
    }

    void Log(std::string const& message)
    {
        m_logger.Write(message);
    }

private:
    SystemLogger m_logger;
};

#endif // APPLICATION_HPP
