# Overview
The factory method can be used to create different objects from a function within a class,
or a base factor class can be extended to provide different versions of the base class factory.

The objects can be created from a function, by parsing some information within the function
to assist the selection of the object type that needs to be created.
