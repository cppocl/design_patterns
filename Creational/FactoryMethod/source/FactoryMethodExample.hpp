#ifndef FACTORYMETHODEXAMPLE_HPP
#define FACTORYMETHODEXAMPLE_HPP

#include "FactoryMethodApplication.hpp"

class FactoryMethodExample
{
public:
    static void Main()
    {
        FactoryMethodApplication application;
        application.ListenToAnimals();
    }
};

#endif // FACTORYMETHODEXAMPLE_HPP
