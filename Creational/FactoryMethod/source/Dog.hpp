#ifndef DOG_HPP
#define DOG_HPP

#include "Animal.hpp"
#include <iostream>

class Dog : public Animal
{
public:
    void Speak() override
    {
        std::cout << "Woof" << std::endl;
    }
};

#endif // DOG_HPP
