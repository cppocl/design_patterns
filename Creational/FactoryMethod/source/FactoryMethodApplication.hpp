#ifndef FACTORYMETHODAPPLICAATION_HPP
#define FACTORYMETHODAPPLICAATION_HPP

#include "Cat.hpp"
#include "Dog.hpp"
#include <list>

class FactoryMethodApplication
{
public:
    FactoryMethodApplication()
    {
        m_animals.push_back(new Cat());
        m_animals.push_back(new Dog());
    }

    void ListenToAnimals()
    {
        for (auto iter : m_animals)
        {
            Animal* animal = iter;
            animal->Speak();
        }
    }

private:
    std::list<Animal*> m_animals;
};

#endif // FACTORYMETHODAPPLICAATION_HPP
