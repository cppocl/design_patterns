#ifndef ANIMALCREATOR_HPP
#define ANIMALCREATOR_HPP

#include "Cat.hpp"
#include "Dog.hpp"
#include <string>

class AnimalCreator
{
public:
    Animal* GetAnimal(std::string animal_type)
    {
        if (animal_type == "cat")
            return new Cat();
        if (animal_type == "dog")
            return new Dog();
        return nullptr;
    }
};

#endif // ANIMALCREATOR_HPP
