#ifndef ANIMAL_HPP
#define ANIMAL_HPP

class Animal
{
public:
    virtual void Speak() = 0;
};

#endif // ANIMAL_HPP
