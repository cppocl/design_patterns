#ifndef SOURCE_CAT_HPP
#define SOURCE_CAT_HPP

#include "Animal.hpp"
#include <iostream>

class Cat : public Animal
{
public:
    void Speak() override
    {
        std::cout << "Meow" << std::endl;
    }
};

#endif // SOURCE_CAT_HPP
