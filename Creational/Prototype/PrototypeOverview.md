# Overview
The prototype pattern provides a design which can reduce the amount of inherited classes that are required,
by implementing an object cloning strategy.
Instead of sub-classing, an existing object can be cloned and then modified.
