#ifndef POLYGON_HPP
#define POLYGON_HPP

#include "Shape.hpp"

class Polygon : public Shape
{
public:
    Shape* Clone() const override
    {
        Polygon* polygon = new Polygon();
        polygon->m_points = m_points;
        return polygon;
    }

    // Could be points for a polygon, or centre point, height and width for ellipse.
    void Set(std::vector<Point> const& points) override
    {
        m_points = points;
    }

    void Move(int X, int Y) override
    {
        for (Point& pt : m_points)
        {
            pt.X += X;
            pt.Y += Y;
        }
    }

    void Draw() override
    {
        std::cout << "Polygon: ";
        for (Point const pt : m_points)
            std::cout << "X,Y: " << pt.X << "," << pt.Y << ". ";
        std::cout << std::endl;
    }

private:
    std::vector<Point> m_points;
};

#endif // POLYGON_HPP
