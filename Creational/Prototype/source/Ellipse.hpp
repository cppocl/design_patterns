#ifndef ELLIPSE_HPP
#define ELLIPSE_HPP

#include "Shape.hpp"
#include <cassert>

class Ellipse : public Shape
{
public:
    Shape* Clone() const override
    {
        Ellipse* ellipse = new Ellipse();
        ellipse->m_centre = m_centre;
        ellipse->m_width  = m_width;
        ellipse->m_height = m_height;
        return ellipse;
    }

    // Could be points for a polygon, or centre point, height and width for ellipse.
    void Set(std::vector<Point> const& centre_width_height) override
    {
        assert(centre_width_height.size() == 2);
        m_centre = centre_width_height[0];
        m_width  = centre_width_height[1].X;
        m_height = centre_width_height[1].Y;
    }

    void Move(int X, int Y) override
    {
        m_centre.X += X;
        m_centre.Y += Y;
    }

    void Draw() override
    {
        std::cout << "Ellipse: ";
        std::cout << "Centre: " << m_centre.X << "," << m_centre.Y;
        std::cout << ". Width: " << m_width << ". Height: " << m_height;
        std::cout << std::endl;
    }

private:
    Point m_centre;
    int m_width;
    int m_height;
};

#endif // ELLIPSE_HPP
