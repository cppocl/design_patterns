#ifndef PROTOTYPEEXAMPLE_HPP
#define PROTOTYPEEXAMPLE_HPP

#include "PrototypeApplication.hpp"

class PrototypeExample
{
public:
    static void Main()
    {
        PrototypeApplication application;
        application.SetShapes();
        application.Draw();
    }
};

#endif // PROTOTYPEEXAMPLE_HPP
