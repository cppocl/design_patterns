#ifndef CLONABLE_HPP
#define CLONABLE_HPP

class Clonable
{
public:
    virtual Clonable* Clone() const = 0;
};

#endif // CLONABLE_HPP
