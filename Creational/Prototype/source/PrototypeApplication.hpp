#ifndef PROTOTYPEAPPLICATION_HPP
#define PROTOTYPEAPPLICATION_HPP

#include "Polygon.hpp"
#include "Ellipse.hpp"

class PrototypeApplication
{
public:
    // Add two polygons and two ellipses.
    void SetShapes()
    {
        Polygon* polygon = new Polygon();

        // 4 points for a 4 sided polygon.
        std::vector<Point> points
        {
            {1, 2},
            {4, 1},
            {2, 3},
            {6, 4}
        };
        polygon->Set(points);
        m_shapes.push_back(polygon);

        // centre, width and height for an ellipse.
        std::vector<Point> centre_width_height
        {
            {3, 5}, // centre X and Y
            {6, 4}  // width and height.
        };
        Ellipse* ellipse = new Ellipse();
        ellipse->Set(centre_width_height);
        m_shapes.push_back(ellipse);

        Shape* polygon2 = polygon->Clone();
        polygon2->Move(2, 3);
        m_shapes.push_back(polygon2);

        Shape* ellipse2 = ellipse->Clone();
        ellipse2->Move(3, 2);
        m_shapes.push_back(ellipse2);
    }

    // Display the points, width and height of all the shapes.
    void Draw()
    {
        for (Shape* shape : m_shapes)
            shape->Draw();
    }

private:
    std::vector<Shape*> m_shapes;
};

#endif // PROTOTYPEAPPLICATION_HPP
