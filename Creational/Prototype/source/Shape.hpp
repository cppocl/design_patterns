#ifndef SHAPE_HPP
#define SHAPE_HPP

#include "Clonable.hpp"
#include "Point.hpp"
#include <vector>
#include <iostream>

// Common clonable shape, that can be used to clone any shapes
// that can be drawn with a series of points.
class Shape : public Clonable
{
public:
    // Create a copy of the shape.
    Shape* Clone() const override = 0;

    // Could be points for a polygon, or centre point, height and width for ellipse.
    virtual void Set(std::vector<Point> const& data) = 0;

    // Move the position of the shape.
    virtual void Move(int X, int Y) = 0;

    virtual void Draw() = 0;
};

#endif // SHAPE_HPP
