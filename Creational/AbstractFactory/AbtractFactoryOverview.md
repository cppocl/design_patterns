# Overview
The abstract factory pattern generates concrete factory classes that can be used to create objects.

The base class for the abstract factory will never have any implementation for creating objects,
and only provide an interface for the derived factories to create objects.

# Recommendations
Use language in naming classes that people will understand and offer context to the problem being solved,
to aid in the readability of the code.

There should be no need to store variables in the factory classes, as they are used for only creating objects.


## Example
If someone wished to visit a local business to have a custom made drinking glass made,
they would discuss with the glass blower about the shape and size.

Naming a class that has real life context, such as GlassBlower provide details that are easier to understand
than naming a class GlassFactory, even though they are both creating a drinking glass.

Taking the principle further for an abstract factory, you could have a Crafter base class, then other crafters
such as Carpenter class could derive from Crafter.

This would result in something like this:

```
       Crafter
          ^
          |
    --------------
    |            |
Carpenter   GlassBlower
```
