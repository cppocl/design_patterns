#ifndef ABSTRACTFACTORYMAIN_HPP
#define ABSTRACTFACTORYMAIN_HPP

#include "GuiApplication.hpp"
#include <string>

class AbstractFactoryExample
{
public:
    static void Main(std::string platform)
    {
        bool is_windows = platform == "windows";

        GuiApplication gui_app(is_windows);
        gui_app.AddButton();
        gui_app.AddTextBox();
        gui_app.Draw();
    }
};

#endif // ABSTRACTFACTORYMAIN_HPP
