#ifndef WINDOWSTEXTBOX_HPP
#define WINDOWSTEXTBOX_HPP

#include "TextBox.hpp"
#include <iostream>

class WindowsTextBox : public TextBox
{
public:
    void Draw() override { std::cout << "Windows text box" << std::endl; }
};

#endif // WINDOWSTEXTBOX_HPP
