#include <string>
#include <iostream>

#include "GuiApplication.hpp"

GuiApplication::GuiApplication(bool is_windows)
{
    if (is_windows)
        m_widget_creator = new WindowsWidgetCreator();
    else
        m_widget_creator = new LinuxWidgetCreator();
}

void GuiApplication::AddButton()
{
    m_widgets.push_back(m_widget_creator->CreateButton());
}

void GuiApplication::AddTextBox()
{
    m_widgets.push_back(m_widget_creator->CreateTextBox());
}

void GuiApplication::Draw()
{
    for (auto iter : m_widgets)
    {
        Widget* widget = iter;
        widget->Draw();
    }
}
