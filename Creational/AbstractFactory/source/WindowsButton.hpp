#ifndef WINDOWSBUTTON_HPP
#define WINDOWSBUTTON_HPP

#include "Button.hpp"
#include <iostream>

class WindowsButton : public Button
{
public:
    void Draw() override { std::cout << "Windows button" << std::endl; }
};

#endif // WINDOWSBUTTON_HPP
