#ifndef LINUXBUTTON_HPP
#define LINUXBUTTON_HPP

#include "Button.hpp"
#include <iostream>

class LinuxButton : public Button
{
public:
    void Draw() override { std::cout << "Linux button" << std::endl; }
};

#endif // LINUXBUTTON_HPP
