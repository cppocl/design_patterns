#ifndef WIDGET_HPP
#define WIDGET_HPP

class Widget
{
public:
    virtual void Draw() = 0;
};

#endif // WIDGET_HPP
