#ifndef LINUXWIDGETCREATOR_HPP
#define LINUXWIDGETCREATOR_HPP

#include "LinuxButton.hpp"
#include "LinuxTextBox.hpp"
#include "PlatformWidgetCreator.hpp"

// Implements the factory for creating Linux widgets.
class LinuxWidgetCreator : public PlatformWidgetCreator
{
public:
    virtual Button* CreateButton() override { return new LinuxButton(); }
    virtual TextBox* CreateTextBox() override { return new LinuxTextBox(); }
};

#endif // LINUXWIDGETCREATOR_HPP
