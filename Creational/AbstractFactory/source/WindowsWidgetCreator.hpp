#ifndef WINDOWSWIDGETCREATOR_HPP
#define WINDOWSWIDGETCREATOR_HPP

#include "WindowsButton.hpp"
#include "WindowsTextBox.hpp"
#include "PlatformWidgetCreator.hpp"

// Implements the factory for creating Windows widgets.
class WindowsWidgetCreator : public PlatformWidgetCreator
{
public:
    virtual Button* CreateButton() override { return new WindowsButton(); }
    virtual TextBox* CreateTextBox() override { return new WindowsTextBox(); }
};

#endif // WINDOWSWIDGETCREATOR_HPP
