#ifndef BUTTON_HPP
#define BUTTON_HPP

#include "Widget.hpp"

class Button : public Widget
{
public:
    void Draw() override = 0;
};

#endif // BUTTON_HPP
