#ifndef TEXTBOX_HPP
#define TEXTBOX_HPP

#include "Widget.hpp"

class TextBox : public Widget
{
public:
    void Draw() override = 0;
};

#endif // TEXTBOX_HPP
