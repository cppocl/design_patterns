#ifndef PLATFORMWIDGETCREATOR_HPP
#define PLATFORMWIDGETCREATOR_HPP

#include "Button.hpp"
#include "TextBox.hpp"

// The base class Factory for creating widgets.
class PlatformWidgetCreator
{
public:
    virtual Button* CreateButton() = 0;
    virtual TextBox* CreateTextBox() = 0;
};

#endif // PLATFORMWIDGETCREATOR_HPP
