#ifndef GUIAPPLICATION_HPP
#define GUIAPPLICATION_HPP

#include "LinuxWidgetCreator.hpp"
#include "WindowsWidgetCreator.hpp"
#include <list>

class GuiApplication
{
public:
    GuiApplication(bool is_windows);

    void AddButton();
    void AddTextBox();
    void Draw();

private:
    PlatformWidgetCreator* m_widget_creator;
    std::list<Widget*> m_widgets;
};

#endif // GUIAPPLICATION_HPP
