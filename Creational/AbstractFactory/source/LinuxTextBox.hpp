#ifndef LINUXTEXTBOX_HPP
#define LINUXTEXTBOX_HPP

#include "TextBox.hpp"
#include <iostream>

class LinuxTextBox : public TextBox
{
public:
    void Draw() override { std::cout << "Linux text box" << std::endl; }
};

#endif // LINUXTEXTBOX_HPP
