/*
Copyright 2021 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "Behavioral/Command/source/CommandExample.hpp"
#include "Creational/AbstractFactory/source/AbstractFactoryExample.hpp"
#include "Creational/FactoryMethod/source/FactoryMethodExample.hpp"
#include "Creational/Singleton/source/SingletonExample.hpp"
#include "Creational/Prototype/source/PrototypeExample.hpp"
#include "Structural/Adapter/source/AdapterExample.hpp"
#include <string>

int main(int argc, char* argv[])
{
    // Abstract factory example.
    std::cout << "Abstract Factory:" << std::endl;
    std::string platform(argc == 2 ? argv[1] : "windows");
    AbstractFactoryExample::Main(platform);
    std::cout << std::endl;

    // Factory method example.
    std::cout << "Factory Method:" << std::endl;
    FactoryMethodExample::Main();
    std::cout << std::endl;

    // Singleton example.
    std::cout << "Singleton:" << std::endl;
    SingletonExample::Main("log.txt");
    std::cout << std::endl;

    // Prototype example.
    std::cout << "Prototype:" << std::endl;
    PrototypeExample::Main();
    std::cout << std::endl;

    // Adapter example.
    std::cout << "Adapter:" << std::endl;
    AdapterExample::Main();
    std::cout << std::endl;

    // Command example.
    std::cout << "Command:" << std::endl;
    CommandExample::Main();
}
