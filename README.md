# Design Patterns

![](header_image.jpg)

## Overview

C++ design pattern examples.

## Table of patterns

| Pattern | Type | Description | References |
|---------|------|-------------|------------|
| Chain of responsibility | Behavioral | N/A | Design Patterns (GOF) |
| [Command](Behavioral/Command/uml/CommandUML.png) | Behavioral | Implement derived Command classes with Execute functions | Design Patterns (GOF) |
| Interpreter | Behavioral | N/A | Design Patterns (GOF) |
| Iterator | Behavioral | N/A | Design Patterns (GOF) |
| Mediator | Behavioral | N/A | Design Patterns (GOF) |
| Memento | Behavioral | N/A | Design Patterns (GOF) |
| [Observer](Behavioral/Observer/uml/ObserverUML.png) | Behavioral | Notify observers of activity, change or updates | Design Patterns (GOF) |
| State | Behavioral | N/A | Design Patterns (GOF) |
| Strategy | Behavioral | N/A | Design Patterns (GOF) |
| Template method | Behavioral | N/A | Design Patterns (GOF) |
| Visitor | Behavioral | N/A | Design Patterns (GOF) |
| [Abstract Factory](Creational/AbstractFactory/uml/AbstractFactoryUML.png) | Creational | Implement classes from an abstract base class that can create objects at runtime | Design Patterns (GOF) |
| Builder | Creational | N/A | Design Patterns (GOF) |
| [Factory Method](Creational/FactoryMethod/uml/FactoryMethodUML.png) | Creational | Implement a function that can conditionally create various objects | Design Patterns (GOF) |
| [Prototype](Creational/Prototype/uml/PrototypeUML.png) | Creational | Implement derived classes that can clone objects, reducing the need for additional sub-classing | Design Patterns (GOF) |
| [Singleton](Creational/Singleton/uml/SingletonUML.png) | Creational | Create a single instance of an object shared across the system | Design Patterns (GOF) |
| [Adapter](Structural/Adapter/uml/AdapterUML.png) | Structural | Provide a class that implements an interface, that wraps two or more other classes, hiding their detail | Design Patterns (GOF) |
| Bridge | Structural | N/A | Design Patterns (GOF) |
| Composite | Structural | N/A | Design Patterns (GOF) |
| Decorator | Structural | N/A | Design Patterns (GOF) |
| Facade | Structural | N/A | Design Patterns (GOF) |
| Flyweight | Structural | N/A | Design Patterns (GOF) |
| Proxy | Structural | N/A | Design Patterns (GOF) |

## References

| Title | Authors | ISBN | Abbreviation |
|-------|---------|------|--------------|
| Design Patterns | Erich Gamma, Richard Helm, Ralph Johnson and John Vlissides | 0-201-63361-2 | Design Patterns (GOF) |
