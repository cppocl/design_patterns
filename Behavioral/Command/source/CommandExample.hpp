#ifndef COMMANDEXAMPLE_HPP
#define COMMANDEXAMPLE_HPP

#include "CommandApplication.hpp"
#include <string>

class CommandExample
{
public:
    static void Main()
    {
        CommandApplication app;
        app.PerformTests();
    }
};

#endif // COMMANDEXAMPLE_HPP
