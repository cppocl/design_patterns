#ifndef TESTINDICATORS_HPP
#define TESTINDICATORS_HPP

#include "Command.hpp"
#include "Vehicle.hpp"
#include <list>
#include <iostream>

class TestIndicators : public Command
{
public:
    TestIndicators(std::list<Vehicle*>& vehicles)
        : m_vehicles(vehicles)
    {
    }

    void Execute() override
    {
        for (auto vehicle : m_vehicles)
        {
            // Test the indicators for each vehicle.
            vehicle->StartIndicator(true);
            if (!vehicle->IsIndicatorBlinking())
                std::cout << "Left indicator failed to start!" << std::endl;

            vehicle->StartIndicator(false);
            if (!vehicle->IsIndicatorBlinking())
                std::cout << "Right indicator failed to start!" << std::endl;

            vehicle->StopIndicator();
            if (vehicle->IsIndicatorBlinking())
                std::cout << "Indicator failed to stop!" << std::endl;
        }
    }

private:
    std::list<Vehicle*>& m_vehicles;

};

#endif // TESTINDICATORS_HPP
