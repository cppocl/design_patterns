#ifndef VEHICLE_HPP
#define VEHICLE_HPP

class Vehicle
{
public:
    virtual void StartEngine() = 0;

    virtual void StopEngine() = 0;

    virtual bool IsEngineRunning() = 0;

    virtual void StartIndicator(bool is_left) = 0;

    virtual void StopIndicator() = 0;

    virtual bool IsIndicatorBlinking() = 0;
};

#endif // VEHICLE_HPP
