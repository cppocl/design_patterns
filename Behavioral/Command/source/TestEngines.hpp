#ifndef TESTENGINES_HPP
#define TESTENGINES_HPP

#include "Command.hpp"
#include "Vehicle.hpp"
#include <list>
#include <iostream>

class TestEngines : public Command
{
public:
    TestEngines(std::list<Vehicle*>& vehicles)
        : m_vehicles(vehicles)
    {
    }

    void Execute() override
    {
        for (auto vehicle : m_vehicles)
        {
            // Test the engine for each vehicle.
            vehicle->StartEngine();
            if (!vehicle->IsEngineRunning())
                std::cout << "Engine failed to start!" << std::endl;

            vehicle->StopEngine();
            if (vehicle->IsEngineRunning())
                std::cout << "Engine failed to stop!" << std::endl;
        }
    }

private:
    std::list<Vehicle*>& m_vehicles;
};

#endif // TESTENGINES_HPP
