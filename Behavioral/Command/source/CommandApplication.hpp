#ifndef COMMANDAPPLICATION_HPP
#define COMMANDAPPLICATION_HPP

#include "Car.hpp"
#include "Motorcycle.hpp"
#include "TestEngines.hpp"
#include "TestIndicators.hpp"
#include <list>

class CommandApplication
{
public:
    CommandApplication()
    {
        m_vehicles.push_back(new Car());
        m_vehicles.push_back(new Motorcycle());

        m_test_commands.push_back(new TestEngines(m_vehicles));
        m_test_commands.push_back(new TestIndicators(m_vehicles));
    }

    void PerformTests()
    {
        for (auto command : m_test_commands)
            command->Execute();
    }

private:
    std::list<Command*> m_test_commands;
    std::list<Vehicle*> m_vehicles;
};

#endif // COMMANDAPPLICATION_HPP
