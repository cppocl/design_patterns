#ifndef MOTORCYCLE_HPP
#define MOTORCYCLE_HPP

#include "Vehicle.hpp"
#include <iostream>

class Motorcycle : public Vehicle
{
public:
    void StartEngine() override
    {
        m_engine_started = true;
        std::cout << "Motorcycle engine started" << std::endl;
    }

    void StopEngine() override
    {
        m_engine_started = false;
        std::cout << "Motorcycle engine stopped" << std::endl;
    }

    bool IsEngineRunning() override
    {
        return m_engine_started;
    }

    void StartIndicator(bool is_left) override
    {
        if (is_left)
        {
            m_left_indicator_blinking = true;
            m_right_indicator_blinking = false;
        }
        else
        {
            m_left_indicator_blinking = false;
            m_right_indicator_blinking = true;
        }

        char const* side = is_left ? "left" : "right";
        std::cout << "Motorcycle " << side << " indicator started" << std::endl;
    }

    void StopIndicator() override
    {
        m_left_indicator_blinking = m_right_indicator_blinking = false;
        std::cout << "Motorcycle indicators stopped" << std::endl;
    }

    bool IsIndicatorBlinking() override
    {
        return m_left_indicator_blinking || m_right_indicator_blinking;
    }

private:
    bool m_engine_started = false;
    bool m_left_indicator_blinking = false;
    bool m_right_indicator_blinking = false;
};

#endif // MOTORCYCLE_HPP
