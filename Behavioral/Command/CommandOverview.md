# Overview
The command pattern allows for an object to perform an action, by implementing a function to perform a series of actions.
This pattern relies on a interface containing an execute function that is then overridden later with the specific series of actions.
