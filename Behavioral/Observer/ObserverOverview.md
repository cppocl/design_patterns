# Overview
The observer pattern allows a subject to notify observers of activity, changes or updates.

One example of this is a car, where the speedometer display shows the current speed and the car also stores speed information, which could be used with accident investigations.
