#ifndef SPEEDOMETER_HPP
#define SPEEDOMETER_HPP

#include "IObserver.hpp"
#include <iostream>

class Speedometer : public IObserver
{
public:
    void update(int speed) override
    {
        std::cout << speed << std::endl;
    }
};

#endif // SPEEDOMETER_HPP
