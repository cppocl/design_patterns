#ifndef CAR_HPP
#define CAR_HPP

#include "SpeedController.hpp"

class Car
{
public:
    Car()
    {
    }

    ~Car()
    {
    }

    void accelerate(int speed)
    {
        if (m_controller.getSpeed() < speed)
            m_controller.setSpeed(speed);
    }

    void decelerate(int speed)
    {
        if (m_controller.getSpeed() > speed)
            m_controller.setSpeed(speed);
    }

    void registerObserver(IObserver& observer)
    {
        m_controller.registerObserver(observer);
    }

private:
    SpeedController m_controller;
};

#endif // CAR_HPP
