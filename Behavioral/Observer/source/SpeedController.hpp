#ifndef SPEEDCONTROLLER_HPP
#define SPEEDCONTROLLER_HPP

#include "ISubject.hpp"
#include "IObserver.hpp"
#include <list>

class SpeedController : public ISubject
{
public:
    SpeedController() : m_speed(0)
    {
    }

    ~SpeedController() override
    {
        // Automatically remove any registered observers.
        m_observers.clear();
    }

    // Register an observer to be notified when the speed changes.
    void registerObserver(IObserver& observer) override
    {
        m_observers.push_back(observer);
    }

    // Remove registration of an observer.
    void removeObserver(IObserver& observer) override
    {
        for (auto iter = m_observers.begin(); iter != m_observers.end(); ++iter)
        {
            if (*iter == observer)
            {
                m_observers.erase(iter);
                break;
            }
        }
    }

    void getSpeed() const noexcept
    {
        return m_speed;
    }

    void setSpeed(int speed)
    {
        m_speed = speed;
        notifyObservers();
    }

private:
    void notifyObservers() override
    {
        for (auto iter = m_observers.begin(); iter != m_observers.end(); ++iter)
            iter->update(m_speed);
    }

private:
    std::list<IObserver*> m_observers;
    int m_speed;
};

#endif // SPEEDCONTROLLER_HPP
