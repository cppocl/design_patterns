#ifndef IOBSERVER_HPP
#define IOBSERVER_HPP

class IObserver
{
public:
    virtual void update(int speed) = 0;
};

#endif // IOBSERVER_HPP
