#ifndef ISUBJECT_HPP
#define ISUBJECT_HPP

class ISubject
{
public:
    // Register an observer to be notified when the speed changes.
    virtual void registerObserver(IObserver& observer) = 0;

    // Remove a previously registered observer.
    virtual void removeObserver(IObserver& observer) = 0;

protected:
    virtual ~ISubject() {}

    // Implement the means to notify registered observers (via update function).
    // This is only called be derived classes after the subject data has changed.
    virtual void notifyObservers() = 0;
};

#endif // ISUBJECT_HPP
