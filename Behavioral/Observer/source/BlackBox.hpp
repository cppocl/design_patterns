#ifndef BLACKBOX_HPP
#define BLACKBOX_HPP

#include "IObserver.hpp"
#include <iostream>

class BlackBox : public IObserver
{
public:
    void update(int speed) override
    {
        std::cout << "Saving speed: " << speed << std::endl;
    }
};

#endif // BLACKBOX_HPP
