#include "Car.hpp"
#include "Speedometer.hpp"
#include "BlackBox.hpp"

int main()
{
    BlackBox blackbox;
    Speedometer speedometer;
    Car car;

    car.registerObserver(blackbox);
    car.registerObserver(speedometer);

    car.accelerate(30);
    car.accelerate(40);
    car.decelerate(20);
    car.decelerate(0);
}
