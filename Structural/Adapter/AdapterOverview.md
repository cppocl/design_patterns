# Overview
The adapter pattern allows a non-compatible interface within a system to be added to a system by providing an adapter to encapsulate the non-compatible interfere with a new compatible adapter interface.

One example of this would be having a 3-pin plug and needing to plugged into 2-pin socket.
This would be solved by creating a 3-pin to 2-pin adapter interface.
