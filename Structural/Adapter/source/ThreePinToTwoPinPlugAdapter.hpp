#ifndef THREEPINTOTWOPINPLUGADAPTER_HPP
#define THREEPINTOTWOPINPLUGADAPTER_HPP

#include "ThreePinPlug.hpp"
#include "ITwoPinPlug.hpp"

class ThreePinToTwoPinPlugAdapter : ITwoPinPlug
{
public:
    ThreePinToTwoPinPlugAdapter(ThreePinPlug& plug) : m_plug(plug)
    {
    }

    void LiveProng() override
    {
        m_plug.LiveProng();
    }

    void NeutralProng() override
    {
        m_plug.NeutralProng();
    }

private:
    ThreePinPlug& m_plug;
};

#endif // THREEPINTOTWOPINPLUGADAPTER_HPP
