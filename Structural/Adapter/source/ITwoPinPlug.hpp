#ifndef ITWOPINPLUG_HPP
#define ITWOPINPLUG_HPP

class ITwoPinPlug
{
public:
    virtual void LiveProng() = 0;

    virtual void NeutralProng() = 0;
};

#endif // ITWOPINPLUG_HPP
