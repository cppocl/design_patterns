#ifndef TWOPINSOCKET_HPP
#define TWOPINSOCKET_HPP

#include "ITwoPinPlug.hpp"

class TwoPinSocket
{
public:
    void LiveHole()
    {
    }

    void NeutralHole()
    {
    }

    void PlugIn(ITwoPinPlug& plug)
    {
        plug.LiveProng();
        plug.NeutralProng();
    }
};

#endif // TWOPINSOCKET_HPP
