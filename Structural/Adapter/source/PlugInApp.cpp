#include "ThreePinPlug.hpp"
#include "TwoPinPlug.hpp"
#include "TwoPinSocket.hpp"
#include "ThreePinToTwoPinPlugAdapter.hpp"

int main()
{
    TwoPinPlug plug2;
    ThreePinPlug plug3;
    TwoPinSocket left_socket;
    TwoPinSocket right_socket;
    ThreePinToTwoPinPlugAdapter adpater(plug3);

    left_socket.PlugIn(plug2);
    right_socket.PlugIn(adpater);
}
